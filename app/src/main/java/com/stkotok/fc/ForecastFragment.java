package com.stkotok.fc;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class ForecastFragment extends Fragment {

    private String location = "Labinsk,RU";

    public ForecastFragment() { /* nothing */ }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);   // This line add menu elements from fragment to activity
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.forecast_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            new FetchWeatherTask().execute(location);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        new FetchWeatherTask().execute(location);

        ArrayList<String> weekForecast = new ArrayList<String>() {{
            add("day 1 - sunny - 15");
            add("day 2 - clear - 16");
            add("day 3 - cloud - 17");
            add("day 4 - rain - 18");
            add("day 5 - snow - 19");
        }};
        ArrayAdapter<String> mForecastAdapter = new ArrayAdapter<>(
                getActivity(),
                R.layout.list_item_forecast,
                R.id.list_item_forecast_textview,
                weekForecast);

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ListView listView = (ListView) rootView.findViewById(R.id.listView_forecast);
        listView.setAdapter(mForecastAdapter);

        return rootView;
    }

    public class FetchWeatherTask extends AsyncTask<String, Void, String> {

        public static final String QUERY_LOCATION_PARAM = "q";
        public static final String DATA_FORMAT_PARAM = "mode";
        public static final String UNITS_FORMAT_PARAM = "units";
        public static final String DAYS_COUNT_PARAM = "cnt";
        private final String LOG_TAG = FetchWeatherTask.class.getSimpleName();

        @Override
        protected String doInBackground(String... params) {  // Inside this method я не должен обращаться к чему-либо из основного потока
            if (params.length == 0) {
                return null;
            }
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            String forecastJsonStr;

            try {
                URL url = new URL(buildUri(params));

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

//                publishProgress();  //

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n"); // This line make debugging a lot easier.
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return null;
                }
                forecastJsonStr = buffer.toString();
                Log.v(LOG_TAG, forecastJsonStr);
            } catch (IOException e) {
                Log.e("LOG_TAG", "Error ", e);
                // If the code didn't successfully get the weather data, there's no point in attempting to parse it.
                return null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }
            return null;
        }

        protected String buildUri(String... params) {
            String q = params[0];
            String data_format = "json";
            String units_format = "metric";
            int numDays = 7;
            /* Alternate way:
            final String FORECAST_BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?";
            Uri.Builder builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon();
            */
            Uri.Builder builtUri = new Uri.Builder();
            builtUri.scheme("http")
                    .authority("api.openweathermap.org")
                    .appendPath("data")
                    .appendPath("2.5")
                    .appendPath("forecast")
                    .appendPath("daily")
                    .appendQueryParameter(QUERY_LOCATION_PARAM, q)
                    .appendQueryParameter(DATA_FORMAT_PARAM, data_format)
                    .appendQueryParameter(UNITS_FORMAT_PARAM, units_format)
                    .appendQueryParameter(DAYS_COUNT_PARAM, Integer.toString(numDays))
                    .appendQueryParameter("APPID", "641efb1f58518b79a244bd2f194f8ab8");
            String fetchWeatherUri = builtUri.build().toString();
            Log.v(LOG_TAG, fetchWeatherUri);
            return fetchWeatherUri;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

}